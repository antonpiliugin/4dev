import { useState, useEffect } from 'react'

export default function useAuth() {
    const [isAuth, setAuth] = useState(false)
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        if (localStorage.getItem('token')) setAuth(true)

        function onStorage(event) {
            setAuth(!!localStorage.getItem('token'))
        }

        window.addEventListener('storage', onStorage)

        setLoading(false)

        return () => {
            window.removeEventListener('storage', onStorage)
        }
    }, [])

    return { isLoading, isAuth }
}
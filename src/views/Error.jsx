import Avatar from '@mui/material/Avatar'
import CssBaseline from '@mui/material/CssBaseline'
import Box from '@mui/material/Box'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import Button from '@mui/material/Button'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { red } from '@mui/material/colors'
import { useNavigate } from 'react-router-dom'

const theme = createTheme()

export default function Error() {

  const navigate = useNavigate()

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" marginBottom={4}>
            Ошибка доступа
          </Typography>
          <Box component="section">
            <Typography component="h1" variant="h5" color={red[500]} marginBottom={4}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam et faucibus sapien. Mauris gravida sed sem vitae cursus. Suspendisse sit amet lacus at eros eleifend tempus sit amet a dui. Integer eget urna ac neque rhoncus vulputate ut et enim.
            </Typography>
            <Button
              type="button"
              fullWidth
              variant="contained"
              size="large"
              onClick={() => navigate(-1)}
            >
              Назад
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  )
}
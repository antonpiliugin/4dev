import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import Container from '@mui/material/Container'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { useForm } from 'react-hook-form'
import { useState } from 'react'
import { red } from '@mui/material/colors'

const theme = createTheme()

export default function SignIn() {
  const [loginError, setLoginError] = useState(false)
  const [disabledButton, setDisabledButton] = useState(false)
  const { register, handleSubmit, formState: { errors } } = useForm({
      mode: 'all',
      defaultValues: {
          login: '',
          password: ''
      }
  })

  const onSubmit = ({ login, password }) => {
    setDisabledButton(true)
    if (login !== 'admin' && password !== 'admin') {
      setLoginError(true)
    } else {
      setLoginError(false)
      localStorage.setItem('token', Math.random().toString())
      window.dispatchEvent(new Event('storage'))
    }
    setDisabledButton(false)
  }
  
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Авторизация
          </Typography>
          <Box component="form" onSubmit={handleSubmit(onSubmit)} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              label="Логин"
              name="login"
              autoComplete="email"
              autoFocus
              error={!!errors.login}
              helperText={!!errors.login && "Необходимо ввести логин"}
              {...register("login", { required: true })}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              autoComplete="current-password"
              error={!!errors.password}
              helperText={!!errors.password && "Необходимо ввести пароль"}
              {...register("password", { required: true })}
            />
            {loginError && <Typography component="h1" variant="h5" color={red[500]}>Неверный логин или пароль!</Typography>}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              size="large"
              sx={{ mt: 3, mb: 2 }}
              disabled={!!errors.login || !!errors.password || disabledButton}
            >
              Войти
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  )
}
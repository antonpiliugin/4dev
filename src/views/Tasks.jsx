import { useSelector, useDispatch } from 'react-redux'
import { changeSort, setModal, createTask, STATUS } from '../store/tasks'
import DialogModal from '../components/DialogModal'
import Container from '@mui/material/Container'
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import TaskColumn from '../components/TaskColumn'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'
import { Button } from '@mui/material'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { grey } from '@mui/material/colors'

const theme = createTheme()

export default function Tasks() {
  const sortBy = useSelector((state) => state.tasks.sortBy)
  const tasks = useSelector((state) => state.tasks.tasks)
  const authors = useSelector((state) => state.tasks.authors)
  const dispatch = useDispatch()

  function getTasksByStatus(status) {
    return tasks.filter((task) => task.status === status).map((task) => {
      const author = authors.find((author) => author.id === task.authorId)
      return { ...task, authorName: author.name || 'Unknown' }
    }).sort((a, b) => {
      const date1 = new Date(a.createdAt)
      const date2 = new Date(b.createdAt)

      return sortBy === 'asc' ? date1 - date2 : date2 - date1
    })
  }

  function newTaskModal() {
    dispatch(setModal({
        type: 'NewModal',
        open: true,
        cancelText: 'Удалить',
        successText: 'Сохранить',
        handleSuccess: () => dispatch(createTask()),
        data: {

        }
    }))
  }

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="md" sx={{ marginTop: 4, marginLeft: 'auto', marginRight: 'auto' }}>
        <Box backgroundColor={grey[200]} flexGrow={1} padding={2} borderRadius={1} marginBottom={2} display="flex" justifyContent="space-between">
          <Button
            type="button"
            variant="contained"
            size="large"
            onClick={newTaskModal}
          >
            Новая задача
          </Button>
          <FormControl>
            <InputLabel>Сортировка по дате</InputLabel>
            <Select
              value={sortBy}
              label="Сортировка по дате"
              onChange={(ev) => dispatch(changeSort(ev.target.value))}
            >
              <MenuItem value="asc">По возрастанию</MenuItem>
              <MenuItem value="desc">По убыванию</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box backgroundColor={grey[200]} flexGrow={1} padding={1} borderRadius={1}>
          <Grid container spacing={1} columns={3}>
            {STATUS.map((status, index) => <TaskColumn key={index} tasks={getTasksByStatus(index)} title={status} titleBgColor={grey[400]} />)}
          </Grid>
        </Box>
      </Container>
      <DialogModal />
    </ThemeProvider>
  )
}
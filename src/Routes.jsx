import { Routes, Route } from 'react-router-dom'
import GuardedRoute from './hoc/GuardedRoute'
import SignIn from './views/SignIn'
import Tasks from './views/Tasks'
import Error from './views/Error'
import useAuth from './hooks/useAuth'

const ROUTES = {
  SIGN_IN: '/',
  ERROR: '/error',
  TASKS: '/tasks'
}

export default function AppRoutes(props) {
  const { isAuth, isLoading } = useAuth()

  if (isLoading) {
    return (
      <Routes>
        <Route path="*" element={<p>Loading...</p>} />
      </Routes>
    )
  }

  return (
    <Routes>
      <Route path={ROUTES.ERROR} element={<Error />} />
      <Route element={<GuardedRoute isAccessible={!isAuth} redirectTo={ROUTES.TASKS}/>}>
				<Route path={ROUTES.SIGN_IN} element={<SignIn />} />
			</Route>
      <Route element={<GuardedRoute isAccessible={isAuth} redirectTo={ROUTES.ERROR} />}>
        <Route path={ROUTES.TASKS} element={<Tasks />} />
      </Route>
      <Route path="*" element={<p>Page Not Found</p>} />
    </Routes>
  )
}
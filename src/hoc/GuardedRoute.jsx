import { Navigate, Outlet } from 'react-router-dom'

export default function GuardedRoute({ isAccessible = false, redirectTo = '/error' }) {
    return isAccessible ? <Outlet /> : <Navigate to={redirectTo} replace />
}
import { createSlice } from '@reduxjs/toolkit'

export const STATUS = [
    'В очереди',
    'В работе',
    'Выполнено'
]

export const PRIORITY = [
    'Низкий',
    'Средний',
    'Высокий'
]

export const tasksSlice = createSlice({
  name: 'tasks',
  initialState: {
    modal: {
        type: null,
        open: false,
        cancelText: null,
        successText: null,
        handleCancel: null,
        handleSuccess: null,
        data: {}
    },
    sortBy: 'desc',
    authors: [
        {
            id: '001',
            name: 'John Smith'
        },
        {
            id: '002',
            name: 'Sarah Lee'
        },
        {
            id: '003',
            name: 'James Wilson'
        },
        {
            id: '004',
            name: 'David Taylor'
        },
        {
            id: '005',
            name: 'Emma Anderson'
        },
        {
            id: '006',
            name: 'Ryan Garcia'
        }
    ],
    tasks: [
        {
            id: '001',
            status: 0,
            priority: 2,
            title: 'Develop website homepage',
            description: 'Create a visually appealing and responsive homepage for the website',
            createdAt: '2021-07-23T10:00:00',
            authorId: '001'
        },
        {
            id: '002',
            status: 1,
            priority: 1,
            title: 'Implement user authentication',
            description: 'Add user authentication feature to the website',
            createdAt: '2021-07-24T14:30:00',
            authorId: '002'
        },
        {
            id: '003',
            status: 2,
            priority: 0,
            title: 'Fix CSS issues on mobile devices',
            description: 'Resolve CSS issues on the website for mobile devices',
            createdAt: '2021-07-25T11:15:00',
            authorId: '001'
        },
        {
            id: '004',
            status: 0,
            priority: 1,
            title: 'Add search functionality',
            description: 'Implement search feature on the website',
            createdAt: '2021-07-26T09:00:00',
            authorId: '001'
        },
        {
            id: '005',
            status: 1,
            priority: 2,
            title: 'Optimize website performance',
            description: 'Improve website loading speed and overall performance',
            createdAt: '2021-07-27T16:45:00',
            authorId: '003'
        },
        {
            id: '006',
            status: 2,
            priority: 0,
            title: 'Fix broken links on the website',
            description: 'Identify and fix broken links on the website',
            createdAt: '2021-07-28T13:20:00',
            authorId: '002'
        },
        {
            id: '007',
            status: 0,
            priority: 1,
            title: 'Create product page',
            description: 'Design and develop a product page for the website',
            createdAt: '2021-07-29T10:30:00',
            authorId: '004'
        },
        {
            id: '008',
            status: 1,
            priority: 2,
            title: 'Implement payment gateway',
            description: 'Add payment gateway to the website for online transactions',
            createdAt: '2021-07-30T14:00:00',
            authorId: '005'
        },
        {
            id: '009',
            status: 2,
            priority: 0,
            title: 'Translate website content',
            description: 'Translate website content to multiple languages',
            createdAt: '2021-07-31T11:45:00',
            authorId: '006'
        },
        {
            id: '010',
            status: 0,
            priority: 1,
            title: 'Design email templates',
            description: 'Create visually appealing email templates for the website',
            createdAt: '2021-08-01T09:15:00',
            authorId: '005'
        }
    ]
  },
  reducers: {
    changeSort: (state, { payload }) => {
        if (payload !== 'asc' && payload !== 'desc') return

        state.sortBy = payload
    },
    setModal: (state, { payload }) => {
        if (typeof payload !== 'object' || payload === null) return
        state.modal = payload
    },
    updateModalData: (state, { payload }) => {
        if (typeof payload !== 'object' || payload === null) return

        state.modal = { ...state.modal, data: { ...state.modal.data, ...payload }}
    },
    resetModal: (state) => {
        state.modal = {
            type: null,
            open: false,
            cancelText: null,
            successText: null,
            handleCancel: null,
            handleSuccess: null,
            data: {}
        }
    },
    updateTask: (state, { payload }) => {
        if (typeof payload !== 'string') return

        const taskIndex = state.tasks.findIndex((task) => task.id === payload)
        if (taskIndex > -1) {
            const oldTask = state.tasks[taskIndex]
            const values = Object.fromEntries(Object.entries(state.modal.data).filter(([key, value]) => typeof value !== 'function'))
            const newTask = { ...oldTask, ...values }
            state.tasks[taskIndex] = newTask
        }
    },
    createTask: (state) => {
        if (typeof state.modal.data !== 'object' || state.modal.data === null) return

        const values = Object.fromEntries(Object.entries(state.modal.data).filter(([key, value]) => typeof value !== 'function'))
        if (!values.title || !values.description || !values.authorId || !(typeof values.status === 'number' && values.status >= 0 && values.status <= 2) || !(typeof values.priority === 'number' && values.priority >= 0 && values.priority <= 2)) return
        const newTask = { ...values, id: Math.random().toString(), createdAt: new Date().toISOString() }
        state.tasks.push(newTask)
    }
  }
})

// Action creators are generated for each case reducer function
export const { changeSort, setModal, updateModalData, updateTask, createTask, resetModal } = tasksSlice.actions

export default tasksSlice.reducer
import { configureStore } from '@reduxjs/toolkit'
import tasks from './tasks'

export default configureStore({
  reducer: {
    tasks
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        // Ignore these action types
        ignoredActions: ['tasks/setModal'],
        // Ignore these field paths in all actions
        //ignoredActionPaths: ['meta.arg', 'payload.timestamp'],
        // Ignore these paths in the state
        ignoredPaths: ['tasks.modal'],
      },
    }),
})
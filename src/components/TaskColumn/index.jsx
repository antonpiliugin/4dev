import Grid from '@mui/material/Grid'
import Box from '@mui/material/Box'
import Stack from '@mui/material/Stack'
import TaskCard from '../TaskCard'

export default function TaskColumn(props) {

    return (
        <Grid item xs={3} md={1}>
            <Box backgroundColor={props.titleBgColor} marginBottom={1} padding={1} borderRadius={1} textAlign="center">
                {props.title}
            </Box>
            <Stack spacing={1}>
                {props.tasks.map((task, index) => <TaskCard task={task} key={index} />)}
            </Stack>
        </Grid>
        )
}
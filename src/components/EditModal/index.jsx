import { useSelector } from 'react-redux'
import { STATUS, PRIORITY } from '../../store/tasks'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'

export default function EditModal(props) {
    const modalData = useSelector((state) => state.tasks.modal.data)

    return (
        <Box>
            <Stack direction="row" spacing={1}>
                <Typography variant="subtitle1" component="h6" fontWeight="bold">
                    Исполнитель
                </Typography>
                <Typography variant="subtitle1" component="div">
                    {modalData.authorName}
                </Typography>
            </Stack>
            <Typography variant="subtitle1" component="h6" fontWeight="bold">
                Описание задачи
            </Typography>
            <Typography variant="subtitle1" component="div" paddingLeft={1} paddingRight={1} marginBottom={2}>
                {modalData.description}
            </Typography>
            <FormControl size="small" fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel>Состояние</InputLabel>
                <Select
                    value={modalData.status}
                    label="Состояние"
                    onChange={modalData.handleStatusChange}
                >
                    {STATUS.map((status, index) => <MenuItem key={index} value={index}>{status}</MenuItem>)}
                </Select>
            </FormControl>
            <FormControl size="small" fullWidth>
                <InputLabel>Приоритет</InputLabel>
                <Select
                    value={modalData.priority}
                    label="Приоритет"
                    onChange={modalData.handlePriorityChange}
                >
                    {PRIORITY.map((priority, index) => <MenuItem key={index} value={index}>{priority}</MenuItem>)}
                </Select>
            </FormControl>
        </Box>
    )
}
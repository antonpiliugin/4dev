import { useSelector, useDispatch } from 'react-redux'
import { updateModalData, STATUS, PRIORITY } from '../../store/tasks'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import InputLabel from '@mui/material/InputLabel'
import MenuItem from '@mui/material/MenuItem'

export default function NewModal(props) {
    const dispatch = useDispatch()
    const authors = useSelector((state) => state.tasks.authors)
    const modalData = useSelector((state) => state.tasks.modal.data)

    function handleTitleChange(event) {
        dispatch(updateModalData({ title: event.target.value }))
    }

    function handleAuthorChange(event) {
        dispatch(updateModalData({ authorId: event.target.value }))
    }

    function handleDescriptionChange(event) {
        dispatch(updateModalData({ description: event.target.value }))
    }

    function handleStatusChange(event) {
        dispatch(updateModalData({ status: event.target.value }))
    }

    function handlePriorityChange(event) {
        dispatch(updateModalData({ priority: event.target.value }))
    }

    return (
        <Box>
            <TextField label="Название" variant="outlined" fullWidth value={modalData.title || ''} onChange={handleTitleChange} sx={{ marginBottom: 2 }} />
            <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel>Исполнитель</InputLabel>
                <Select
                    value={modalData.authorId || ''}
                    label="Исполнитель"
                    onChange={handleAuthorChange}
                >
                    {authors.map((author) => <MenuItem key={author.id} value={author.id}>{author.name}</MenuItem>)}
                </Select>
            </FormControl>
            <TextField label="Описание задачи" variant="outlined" fullWidth value={modalData.description || ''} onChange={handleDescriptionChange} sx={{ marginBottom: 2 }} rows={4} multiline />
            <FormControl fullWidth sx={{ marginBottom: 2 }}>
                <InputLabel>Состояние</InputLabel>
                <Select
                    value={typeof modalData.status === 'number' ? modalData.status : ''}
                    label="Состояние"
                    onChange={handleStatusChange}
                >
                    {STATUS.map((status, index) => <MenuItem key={index} value={index}>{status}</MenuItem>)}
                </Select>
            </FormControl>
            <FormControl fullWidth>
                <InputLabel>Приоритет</InputLabel>
                <Select
                    value={typeof modalData.priority === 'number' ? modalData.priority : ''}
                    label="Приоритет"
                    onChange={handlePriorityChange}
                >
                    {PRIORITY.map((priority, index) => <MenuItem key={index} value={index}>{priority}</MenuItem>)}
                </Select>
            </FormControl>
        </Box>
    )
}
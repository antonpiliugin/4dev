import { useDispatch, useSelector } from 'react-redux'
import { setModal, updateModalData, updateTask } from '../../store/tasks'
import CircleIcon from '@mui/icons-material/Circle'
import Grid from '@mui/material/Grid'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import Stack from '@mui/material/Stack'

const colors = ['success', 'warning', 'error']

export default function TaskCard(props) {
    const { id, status, priority, title, authorName, description } = props.task
    const modalData = useSelector((state) => state.tasks.modal.data)

    const dispatch = useDispatch()
    const color = colors[priority]

    function handleStatusChange(event) {
        dispatch(updateModalData({ status: event.target.value }))
    }

    function handlePriorityChange(event) {
        dispatch(updateModalData({ priority: event.target.value }))
    }

    function handleSuccess() {
        dispatch(updateTask(id))
    }

    function detailsModal() {
        dispatch(setModal({
            type: 'EditModal',
            open: true,
            cancelText: 'Отмена',
            successText: 'Сохранить',
            handleSuccess,
            data: {
                id,
                title,
                authorName,
                description,
                status,
                priority,
                handleStatusChange,
                handlePriorityChange
            }
        }))
    }

    return (
        <Paper sx={{ cursor: 'pointer' }} onClick={detailsModal}>
            <Grid container columns={1} flexDirection="column" alignItems="flex-start" padding={1} spacing={1}>
                <Grid item>
                    <Stack direction="row" spacing={1}>
                        <CircleIcon color={color} />
                        <Typography variant="subtitle1" component="div">
                            {title}
                        </Typography>
                    </Stack>
                </Grid>
                <Grid item width="100%">
                    <Typography variant="subtitle2" component="div" align="right">
                        {authorName}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
        )
}
import { createElement } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { resetModal } from '../../store/tasks'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogTitle from '@mui/material/DialogTitle'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import IconButton from '@mui/material/IconButton'
import CloseIcon from '@mui/icons-material/Close'
import EditModal from '../EditModal'
import NewModal from '../NewModal'
import { grey } from '@mui/material/colors'

function BootstrapDialogTitle(props) {
  const { children, onClose, ...other } = props

  return (
    <DialogTitle sx={{ m: 0, p: 2, pr: 8 }} {...other}>
      {children}
      {onClose ? (
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
            color: grey[500],
          }}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  )
}

export default function DialogModal(props) {
  const modal = useSelector((state) => state.tasks.modal)
  const dispatch = useDispatch()

  const handleCancel = () => {
    if (typeof modal.handleCancel === 'function') modal.handleCancel()
    dispatch(resetModal())
  }

  const handleSuccess = () => {
    if (typeof modal.handleSuccess === 'function') modal.handleSuccess()
    dispatch(resetModal())
  }

  const modals = {
    EditModal,
    NewModal
  }

  const modalBody = createElement(modals[modal.type] || 'div')

  return (
    <Dialog
      onClose={handleCancel}
      aria-labelledby="customized-dialog-title"
      open={modal.open}
    >
      <BootstrapDialogTitle id="customized-dialog-title" onClose={handleCancel}>
        {modal.data.title || 'Название задачи'}
      </BootstrapDialogTitle>
      <DialogContent dividers>
        {modalBody}
      </DialogContent>
      <DialogActions>
        <Button variant="contained" color="error" onClick={handleCancel}>
          {modal.cancelText}
        </Button>
        <Button  variant="contained" color="success" onClick={handleSuccess}>
          {modal.successText}
        </Button>
      </DialogActions>
    </Dialog>
  )
}